﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadMainGameAnim : MonoBehaviour {

	public GameObject BlackScreen;

	// Use this for initialization
	public void OnGameLoad () {
		BlackScreen.GetComponent<Animator>().enabled = true;
	}
	
	// Update is called once per frame
	public void Quit() {
		Application.Quit();
	}
}
