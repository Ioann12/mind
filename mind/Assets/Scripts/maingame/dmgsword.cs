﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dmgsword : MonoBehaviour {

	protected float UpdateTimeToDeleteCopyOfKatana = 0.0f;
	protected float SparkTimeToDelete = 0.0f;

	public GameObject DmgKatanaUpAnim;
	public GameObject DmgKatanaUp;
	public GameObject GameOverText;
	public GameObject Spark;
	public float thrust;
	bool IsFall = false;
	
	// Update is called once per frame

	void OnTriggerEnter(Collider other) {
		Debug.Log("Ok");
		if(other.tag == "Player") {
			Debug.Log("OK");
			IsFall = true;
			DmgKatanaUpAnim.GetComponent<Animator>().enabled = false;
			DmgKatanaUp.GetComponent<Rigidbody>().isKinematic = false;
			DmgKatanaUp.GetComponent<Rigidbody>().AddForce(thrust / 2, thrust / 2, thrust, ForceMode.Impulse);
		}
 	}
	private void Update() {		
		if(IsFall) {
			Spark.SetActive(true);
			SparkTimeToDelete += Time.deltaTime;
			UpdateTimeToDeleteCopyOfKatana += Time.deltaTime;

			if (SparkTimeToDelete >= 0.3f)
				Spark.SetActive(false);

			if(UpdateTimeToDeleteCopyOfKatana >= 5.0f) {
				Destroy(DmgKatanaUp);
				UpdateTimeToDeleteCopyOfKatana = 0.0f;
				Debug.Log("Delete");
			}
		}
	}
}
