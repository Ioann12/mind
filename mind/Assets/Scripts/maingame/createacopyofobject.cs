﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class createacopyofobject : MonoBehaviour {

	public GameObject MainKatana;
	GameObject MainKatanaClone;
	float UpdateTime = 0.0f;
	float FixedTime = 0.0f;
	float SpeedAnim = 1.0f;
	float needTime = 5.0f;
	int AnimState;
	int multiplayer = 1;
	public GameObject KataPlayer;

	void Update () {
		UpdateTime += Time.deltaTime;
		FixedTime += Time.deltaTime; 
		
		if (FixedTime > needTime && MainKatana.GetComponent<Animator>().speed <= 4 && KataPlayer.GetComponent<Animator>().speed <= 4) {
			MainKatana.GetComponent<Animator>().speed += 0.2F;
			KataPlayer.GetComponent<Animator>().speed += 0.2F;
			needTime += 5.0f;
			FixedTime = 0.0f;
		}

		if(UpdateTime > 1.0f) {
			MainKatanaClone = Instantiate(MainKatana, transform.position, transform.rotation);
			MainKatanaClone.GetComponent<MeshRenderer>().enabled = true;
			MainKatanaClone.GetComponent<Animator>().enabled = true;
			MainKatanaClone.GetComponent<Animator>().speed = MainKatana.GetComponent<Animator>().speed;

			AnimState = Random.Range(0, 3);
			switch (AnimState) {
				case 0:
					MainKatanaClone.GetComponent<Animator>().SetTrigger("up");
					break;
				case 1:
					MainKatanaClone.GetComponent<Animator>().SetTrigger("right");
					break;
				case 2:
					MainKatanaClone.GetComponent<Animator>().SetTrigger("left");
					break;
			}
			UpdateTime = 0.0f;
		}
	}
}
