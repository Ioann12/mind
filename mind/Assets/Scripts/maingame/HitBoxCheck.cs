﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitBoxCheck : MonoBehaviour {

	public GameObject healthBar;
	public GameObject GameOverAnim;
	public GameObject EnemySword;
	public GameObject MainCamera;

	public int health = 3;
	public float thrust;
	
	public void OnTriggerEnter(Collider other) {
		if (other.tag == "DmgSword") {
			health = health - 1;
			healthBar.GetComponent<Slider>().value -= 1;
			Debug.Log("Dmg");
			if (health == 0) {
				healthBar.SetActive(false);
				EnemySword.SetActive(false);
				GameOverAnim.SetActive(true);
				MainCamera.GetComponent<Score>().enabled = false;
				MainCamera.GetComponent<Rigidbody>().isKinematic = false;
				MainCamera.GetComponent<Rigidbody>().AddForce(thrust / 2, thrust / 2, thrust, ForceMode.Impulse);
			}
		}
	}
}
