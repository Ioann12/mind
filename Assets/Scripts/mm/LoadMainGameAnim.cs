﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadMainGameAnim : MonoBehaviour {

	public GameObject BlackScreen;
	public GameObject Camera;
	public GameObject mindText;
	public GameObject Buttons;
	public GameObject BackButton;
	public GameObject ManualBoard;

	// Use this for initialization
	public void OnGameLoad () {
		BlackScreen.GetComponent<Animator>().enabled = true;
	}
	
	// Update is called once per frame
	public void Quit() {
		Application.Quit();
	}
	
	public void swordChangeUp() {
		BackButton.SetActive(true);
		Camera.GetComponent<Animator>().SetTrigger("up");
		mindText.SetActive(false);
		Buttons.SetActive(false);
	}

	public void swordChangeBack() {
		Camera.GetComponent<Animator>().SetTrigger("down");
		mindText.SetActive(true);
		BackButton.SetActive(false);
		Buttons.SetActive(true);
	}

	public void Manual() {
		Camera.GetComponent<Animator>().SetTrigger("upmanual");
		mindText.SetActive(false);
		Buttons.SetActive(false);
		ManualBoard.SetActive(true);
	}

	public void ManualBack() {
		Camera.GetComponent<Animator>().SetTrigger("downmanual");
		mindText.SetActive(true);
		Buttons.SetActive(true);
		ManualBoard.SetActive(false);
	}
}
