﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSwordSecond : MonoBehaviour {

	public GameObject FireBallFirst;
	public GameObject FireBallSecond;
	public GameObject FireBallThrid;

void Update() {
	if(PlayerPrefs.GetString("secondsword") == "CanToUse") {
		FireBallFirst.SetActive(false);
		FireBallSecond.SetActive(true);
		FireBallThrid.SetActive(false);
	}
}

void OnMouseOver() {
	if (Input.GetKeyDown(KeyCode.Mouse0)) {
			PlayerPrefs.SetString("startsword", "CantToUse");
			PlayerPrefs.SetString("secondsword","CanToUse");
			PlayerPrefs.SetString("thridsword", "CantToUse");
		}
	}
}
