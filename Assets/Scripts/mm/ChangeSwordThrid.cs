﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSwordThrid : MonoBehaviour {

	public GameObject FireBallFirst;
	public GameObject FireBallSecond;
	public GameObject FireBallThrid;

void Update() {
	if(PlayerPrefs.GetString("thridsword") == "CanToUse") {
		FireBallFirst.SetActive(false);
		FireBallSecond.SetActive(false);
		FireBallThrid.SetActive(true);
	}
}

void OnMouseOver() {
	if (Input.GetKeyDown(KeyCode.Mouse0)) {
			PlayerPrefs.SetString("startsword", "CantToUse");
			PlayerPrefs.SetString("secondsword","CantToUse");
			PlayerPrefs.SetString("thridsword", "CanToUse");
		}
	}
}
