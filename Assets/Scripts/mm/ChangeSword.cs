﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSword : MonoBehaviour {

	public GameObject FireBallFirst;
	public GameObject FireBallSecond;
	public GameObject FireBallThrid;

void Update() {
	if(PlayerPrefs.GetString("startsword") == "CanToUse") {
		FireBallFirst.SetActive(true);
		FireBallSecond.SetActive(false);
		FireBallThrid.SetActive(false);
	}
}

void OnMouseOver() {
	if (Input.GetKeyDown(KeyCode.Mouse0)) {
			PlayerPrefs.SetString("startsword", "CanToUse");
			PlayerPrefs.SetString("secondsword","CantToUse");
			PlayerPrefs.SetString("thridsword", "CantToUse");
		}
	}
}
