﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class createacopyofobject : MonoBehaviour {

	public GameObject MainKatana;
	public GameObject KataPlayer;
	GameObject MainKatanaClone;
	float UpdateTime = 0.0f;
	public float needTimeToUpdate = 1.0f;
	float FixedTime = 0.0f;
	public float needTime = 10.0f;
	int AnimState;


	void Update () {
		UpdateTime += Time.deltaTime;
		FixedTime += Time.deltaTime; 
		
		if (FixedTime > needTime && MainKatana.GetComponent<Animator>().speed <= 2.1 && KataPlayer.GetComponent<Animator>().speed <= 2.1) {
			MainKatana.GetComponent<Animator>().speed += 0.1f;
			KataPlayer.GetComponent<Animator>().speed += 0.1f;
			if (needTimeToUpdate <= 0.4f) 
				return;
			else 	
				needTimeToUpdate -= 0.1f;
			FixedTime = 0.0f;
			Debug.Log(MainKatana.GetComponent<Animator>().speed);
			Debug.Log(needTimeToUpdate);
		}

		if(UpdateTime > needTimeToUpdate) {
			MainKatanaClone = Instantiate(MainKatana, transform.position, transform.rotation);
			MainKatanaClone.GetComponent<MeshRenderer>().enabled = true;
			MainKatanaClone.GetComponent<Animator>().enabled = true;
			MainKatanaClone.GetComponent<Animator>().speed = MainKatana.GetComponent<Animator>().speed;

			AnimState = Random.Range(0, 3);
			switch (AnimState) {
				case 0:
					MainKatanaClone.GetComponent<Animator>().SetTrigger("up");
					break;
				case 1:
					MainKatanaClone.GetComponent<Animator>().SetTrigger("right");
					break;
				case 2:
					MainKatanaClone.GetComponent<Animator>().SetTrigger("left");
					break;
			}
			UpdateTime = 0.0f;
		}
	}
}
