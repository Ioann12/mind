﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playerpref : MonoBehaviour {

	public Mesh startsword;
	public Mesh secondsword;
	public Mesh thridsword;
	public GameObject PlayerSword;
	public Material emiss;
	// Use this for initialization
	void Start () {
		if (PlayerPrefs.GetString("startsword") == "CanToUse")
			PlayerSword.GetComponent<MeshFilter>().mesh = startsword;

		else if (PlayerPrefs.GetString("secondsword") == "CanToUse")
			PlayerSword.GetComponent<MeshFilter>().mesh = secondsword;
		
		else if (PlayerPrefs.GetString("thridsword") == "CanToUse") {
			PlayerSword.GetComponent<MeshFilter>().mesh = thridsword;
			PlayerSword.GetComponent<MeshRenderer>().material = emiss;
		}
	}
}
