﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitBoxCheck : MonoBehaviour {

	public GameObject healthBar;
	public GameObject GameOverAnim;
	public GameObject EnemySword;
	public GameObject MainCamera;
	public GameObject Heart;
	public GameObject TimeWarp;
	public GameObject Shield;
	public GameObject Bomb;
	public GameObject EnemyKatana;
	public GameObject Player;
	public GameObject fillAreaOfHealthBar;
	public GameObject sparkOfBomb;
	public GameObject Explosion;
	GameObject heartClone;
	GameObject timeWarpClone;
	GameObject shieldClone;
	GameObject bombClone;

	public int health = 3;
	public float thrust;
	int animStateToHeart;
	int AnimstateToTimeWarp;
	int animStateToShield;
	int animStateToBomb;
	float updateTimeToHeart = 0.0f;
	float updateTimeToTimeWarp = 0.0f;
	float updateTimeToShield = 0.0f;
	float updateTimeToBomb = 0.0f;
	float randomNumToUpdateTimeToShield;	
	float randomNumToUpdateTimeToHeart;
	float randomNumToUpdateTimeWarp;
	float randomNumToUpdateTimeToBomb;
	bool isShieldActive = false;
	bool isBombActive = false;
	
	void Start() {
		randomNumToUpdateTimeToBomb = Random.Range(7.0f, 10.0f);
		randomNumToUpdateTimeToHeart = Random.Range(20.0f, 60.0f);
		randomNumToUpdateTimeWarp = Random.Range(15.0f, 25.0f);
		randomNumToUpdateTimeToShield = Random.Range(20.0f, 60.0f);
	}

	public void OnTriggerEnter(Collider other) {
		if (other.tag == "DmgSword" && !EnemyKatana.GetComponent<dmgsword>().IsFall) {
			if (!isShieldActive) {
				health = health - 1;
				healthBar.GetComponent<Slider>().value -= 1;

			}
			else {
				fillAreaOfHealthBar.GetComponent<Image>().color = Color.red;
				isShieldActive = false;
			}
		}
		if (other.tag == "Heart") {
			if (health < 3)
				health += 1;
			if (healthBar.GetComponent<Slider>().value < 3)
				healthBar.GetComponent<Slider>().value += 1;
			randomNumToUpdateTimeToHeart = Random.Range(20.0f, 60.0f);
			Destroy(heartClone);
		}
		if (other.tag == "timeWarp") {	
			MainCamera.GetComponent<createacopyofobject>().needTimeToUpdate += 0.1f;
			EnemyKatana.GetComponent<Animator>().speed -= 0.1f;
			randomNumToUpdateTimeWarp = Random.Range(15.0f, 40.0f);
			Destroy(timeWarpClone);
		}
		if (other.tag == "Shield" && !isShieldActive) {
			isShieldActive = true;
			fillAreaOfHealthBar.GetComponent<Image>().color = Color.yellow;
			Destroy(shieldClone);
		}
		if (other.tag == "Bomb" ) {
			MainCamera.GetComponent<Rigidbody>().AddForce(thrust + 5, thrust + 5, thrust, ForceMode.Impulse);
			Explosion.SetActive(true);
			health = 0;
			Destroy(bombClone);
		}
		if (health == 0) {
			TimeWarp.SetActive(false);
			Bomb.SetActive(false);
			Heart.SetActive(false);
			healthBar.SetActive(false);
			EnemySword.SetActive(false);
			GameOverAnim.SetActive(true);
			MainCamera.GetComponent<Score>().enabled = false;
			MainCamera.GetComponent<Rigidbody>().isKinematic = false;
			MainCamera.GetComponent<Rigidbody>().AddForce(thrust / 2, thrust / 2, thrust, ForceMode.Impulse);
		}
	}
	void Update() {
		updateTimeToBomb += Time.deltaTime;
		updateTimeToShield += Time.deltaTime;
		updateTimeToHeart += Time.deltaTime;
		updateTimeToTimeWarp += Time.deltaTime;
		animStateToHeart = Random.Range(0, 3);

		if (updateTimeToHeart > randomNumToUpdateTimeToHeart) {
			heartClone = Instantiate(Heart, transform.position, transform.rotation);
			heartClone.GetComponent<MeshRenderer>().enabled = true;
			heartClone.GetComponent<Animator>().enabled = true;

			switch(animStateToHeart) {
				case 0:
					heartClone.GetComponent<Animator>().SetTrigger("left");
					break;
				case 1:
					heartClone.GetComponent<Animator>().SetTrigger("right");
					break;
				case 2:
					heartClone.GetComponent<Animator>().SetTrigger("up");
					break;
			}
			updateTimeToHeart = 0.0f;
		}
		if (updateTimeToTimeWarp > randomNumToUpdateTimeWarp) {
			AnimstateToTimeWarp = Random.Range(0, 3);
			timeWarpClone = Instantiate(TimeWarp, transform.position, transform.rotation);
			timeWarpClone.GetComponent<MeshRenderer>().enabled = true;
			timeWarpClone.GetComponent<Animator>().enabled = true;

			switch(AnimstateToTimeWarp) {
				case 0:
					timeWarpClone.GetComponent<Animator>().SetTrigger("left");
					break;
				case 1:
					timeWarpClone.GetComponent<Animator>().SetTrigger("right");
					break;
				case 2:
					timeWarpClone.GetComponent<Animator>().SetTrigger("up");
					break;
			}
			updateTimeToTimeWarp = 0.0f;
		}
		if (updateTimeToShield > randomNumToUpdateTimeToShield) {
			animStateToShield = Random.Range(0, 3);
			shieldClone = Instantiate(Shield, transform.position, transform.rotation);
			shieldClone.GetComponent<MeshRenderer>().enabled = true;
			shieldClone.GetComponent<Animator>().enabled = true;

			switch(animStateToShield) {
				case 0:
					shieldClone.GetComponent<Animator>().SetTrigger("left");
					break;
				case 1:
					shieldClone.GetComponent<Animator>().SetTrigger("right");
					break;
				case 2:
					shieldClone.GetComponent<Animator>().SetTrigger("up");
					break;
			}
			updateTimeToShield = 0.0f;
		}
		if (updateTimeToBomb > randomNumToUpdateTimeToBomb) {
			animStateToBomb = Random.Range(0, 3);
			bombClone = Instantiate(Bomb, transform.position, transform.rotation);
			bombClone.GetComponent<MeshRenderer>().enabled = true;
			bombClone.GetComponent<Animator>().enabled = true;
			sparkOfBomb.SetActive(true);

			switch(animStateToBomb) {
				case 0:
					bombClone.GetComponent<Animator>().SetTrigger("left");
					break;
				case 1:
					bombClone.GetComponent<Animator>().SetTrigger("right");
					break;
				case 2:
					bombClone.GetComponent<Animator>().SetTrigger("up");
					break;
			}
			isBombActive = true;
			updateTimeToBomb = 0.0f;
		}
		if(isBombActive && Input.GetKeyDown(KeyCode.Space)) {
			sparkOfBomb.SetActive(false);
			bombClone.GetComponent<Animator>().enabled = false;
			bombClone.GetComponent<Rigidbody>().isKinematic = false;
			bombClone.GetComponent<Rigidbody>().AddForce(thrust, thrust, thrust, ForceMode.Impulse);
		} 
	}
}
