﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sword : MonoBehaviour {

	public GameObject Sword;

	public bool UpTrue = false;
	public bool RightTrue = false;
	public bool LeftTrue = false;

	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.UpArrow)) {
			if (!UpTrue) {
				Sword.GetComponent<BoxCollider>().enabled = true;
				Sword.GetComponent<Animator>().SetTrigger("up");
				RightTrue = false;
				LeftTrue = false;
				UpTrue = true;
			}
		}
		else if(Input.GetKeyDown(KeyCode.RightArrow)) {
			if (!RightTrue) {
				Sword.GetComponent<BoxCollider>().enabled = true;
				Sword.GetComponent<Animator>().SetTrigger("right");
				UpTrue = false;
				LeftTrue = false;
				RightTrue = true;
			}
		}
		else if(Input.GetKeyDown(KeyCode.LeftArrow)) {
			if (!LeftTrue) {
				Sword.GetComponent<BoxCollider>().enabled = true;
				Sword.GetComponent<Animator>().SetTrigger("left");
				UpTrue = false;
				RightTrue = false;
				LeftTrue = true;
			}
		}
	}
}
